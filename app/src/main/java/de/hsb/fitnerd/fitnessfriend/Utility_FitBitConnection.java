package de.hsb.fitnerd.fitnessfriend;

import android.os.AsyncTask;


import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by alex on 12.05.2016.
 */
public class Utility_FitBitConnection extends AsyncTask<Utility_FitBitConnection.GetType,Void, String> {

    private final String AppID = "227MBS";
    private final String AppBase64Pass = "MjI3TUJTOjBkODE3NzI5YjM5ZGRkYTJmODEyZGM1ODZkZGVhZGQz";
    private final String UserID = "3XVLYZ";
    private final String UserToken = "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE0OTQ1Mjk4NTAsInNjb3BlcyI6InJ3ZWkgcnBybyByaHIgcmxvYyBybnV0IHJzbGUgcnNldCByYWN0IHJzb2MiLCJzdWIiOiIzWFZMWVoiLCJhdWQiOiIyMjdNQlMiLCJpc3MiOiJGaXRiaXQiLCJ0eXAiOiJhY2Nlc3NfdG9rZW4iLCJpYXQiOjE0NjI5OTM4NTB9.qPdECYdeT2zD0apKF5nGpIsXKpcKKZwtbq01miXCov0";
    private final String UserProfilURL = "https://api.fitbit.com/1/user/"+this.UserID+"/profile.json";
    private final String UserHeartTodayURL = "https://api.fitbit.com/1/user/"+this.UserID+"/activities/heart/date/today/1d/1min.json";

    public enum GetType{
        PROFILEDATA,
        HEARTTODAY
    }

    /**
     * Create a connection to FitBit with the given URL.
     * @param URL The URL with the request
     * @return If successful, returns the response json
     */
    private String getUserData(String URL){
        HttpURLConnection urlConnection = null;
        String returnString = "";
        URL url;

        try {
            url = new URL(URL);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Authorization","Bearer "+this.UserToken);

            InputStream in = urlConnection.getInputStream();
            InputStreamReader reader = new InputStreamReader(in);

            int data = reader.read();
            while (data != -1) {
                char current = (char) data;
                data = reader.read();
                returnString+= current;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return returnString;
    }

    /**
     * Create a connection to Fitbit an returns the data of the given GetType. Needs a GetType as first parameter.
     * @param params The requested Type of data.
     * @return Returns a json string on success
     */
    @Override
    protected String doInBackground(GetType... params) {
        String returnValue = "";

        switch (params[0]){
            case PROFILEDATA:
                returnValue = this.getUserData(this.UserProfilURL);
                break;
            case HEARTTODAY:
                returnValue = this.getUserData(this.UserHeartTodayURL);
        }
        return returnValue;
    }

}
