package de.hsb.fitnerd.fitnessfriend;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.OnDataPointListener;
import com.google.android.gms.fitness.request.SensorRequest;
import com.google.android.gms.fitness.result.DataSourcesResult;

import java.util.concurrent.TimeUnit;

/**
 * Created by Alex on 13.06.2016.
 */
public class Utility_GoogleFitSensors {

    private static Utility_GoogleFitSensors self;
    private String TAG = "GoogleFit";

    private Utility_GoogleFitSensors() {}

    public static Utility_GoogleFitSensors getInstance() {

        if(self == null){
            self = new Utility_GoogleFitSensors();
        }

        return self;
    }

    public void findDataSources(final GoogleApiClient mApiClient, final OnDataPointListener callback) {
        Fitness.SensorsApi.findDataSources(mApiClient, new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_LOCATION_SAMPLE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build()).setResultCallback(new ResultCallback<DataSourcesResult>() {
            @Override
            public void onResult(@NonNull DataSourcesResult dataSourcesResult) {
                Log.i(TAG, "Result: " + dataSourcesResult.getStatus().toString());
                for (DataSource dataSource : dataSourcesResult.getDataSources()) {
                    Log.i(TAG, "Data source found: " + dataSource.toString());
                    Log.i(TAG, "Data Source type: " + dataSource.getDataType().getName());

                    //registering a listener to receive Activity data
                    if (dataSource.getDataType().equals(DataType.TYPE_LOCATION_SAMPLE)
                            && callback != null) {
                        Log.i(TAG, "Data source for LOCATION_SAMPLE found!  Registering.");
                        registerFitnessDataListener(mApiClient, callback,dataSource,
                                DataType.TYPE_LOCATION_SAMPLE);
                    }
                }
            }
        });
    }


    public void create_StepSensor(final GoogleApiClient mApiClient, final OnDataPointListener callback) {
        DataSourcesRequest dataSourceRequest = new DataSourcesRequest.Builder()
                .setDataTypes(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .setDataSourceTypes(DataSource.TYPE_RAW)
                .build();

        ResultCallback<DataSourcesResult> dataSourcesResultCallback = new ResultCallback<DataSourcesResult>() {
            @Override
            public void onResult(DataSourcesResult dataSourcesResult) {
                for (DataSource dataSource : dataSourcesResult.getDataSources()) {
                    if (DataType.TYPE_STEP_COUNT_CUMULATIVE.equals(dataSource.getDataType())) {
                        registerFitnessDataListener(mApiClient, callback, dataSource, DataType.TYPE_STEP_COUNT_CUMULATIVE);
                    }
                }
            }
        };

        Fitness.SensorsApi.findDataSources(mApiClient, dataSourceRequest)
                .setResultCallback(dataSourcesResultCallback);
    }

    //check every three seconds if the listener was triggered
    public void registerFitnessDataListener(GoogleApiClient mApiClient, OnDataPointListener  mDataPointListener,DataSource dataSource, DataType dataType) {
        SensorRequest request = new SensorRequest.Builder()
                .setDataSource(dataSource)
                .setDataType(dataType)
                .setSamplingRate(3, TimeUnit.SECONDS)
                .build();

        Fitness.SensorsApi.add(mApiClient, request, mDataPointListener)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.e("GoogleFit", "SensorApi successfully added");
                        }
                    }
                });
    }

}
