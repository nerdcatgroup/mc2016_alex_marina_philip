package de.hsb.fitnerd.fitnessfriend;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.OnDataPointListener;

import de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart.ActivitiesHeartIntraday;

public class Activity_GoogleFitLogin extends AppCompatActivity  implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnDataPointListener, ActivityCompat.OnRequestPermissionsResultCallback {

    public static final String GOOGLEFITCLIENT = "GOOGLEFITCLIENT-MAPICLIENT";
    private static final int REQUEST_OAUTH = 1;
    private static final String AUTH_PENDING = "auth_state_pending";
    final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 666;
    final int MY_PERMISSIONS_REQUEST_BODY_SENSORS = 333;
    private static final String TAG = Activity_GoogleFitLogin.class.getSimpleName();
    private Context currentContext;
    private boolean authInProgress = false;
    public static GoogleApiClient mApiClient;
    private ActivitiesHeartIntraday heartDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_fit_login);

        //Google Fit API Client
        if( savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }

        Button loginButton = (Button) findViewById(R.id.googleFitLoginButton);
        assert loginButton != null;
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mApiClient.connect(1);
            }
        });

        this.createApiClient();
    }

    public boolean createApiClient(){
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.SENSORS_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                .addScope(new Scope(Scopes.FITNESS_NUTRITION_READ_WRITE))
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        return true;
    }

    @Override
    protected void onStart(){
        super.onStart();
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.BODY_SENSORS)
                        != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BODY_SENSORS},
                        MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permission, int[] grantResults){
        //super.onRequestPermissionsResult(requestCode, permission, grantResults);
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Log.e(TAG, "Access to fine location was granted.");
                    mApiClient.connect();
                }else {
                    Log.e(TAG, "Access to fine location was denied.");
                }
                return;
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_PENDING, authInProgress);
    }

    @Override
    public void onDataPoint(DataPoint dataPoint){
        Log.e("GoogleFit", "Got DataPoint");
        for(final Field field : dataPoint.getDataType().getFields()){
            final Value value = dataPoint.getValue(field);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Field: " + field.getName() + " Value: " + value, Toast.LENGTH_SHORT).show();
                }
            });

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_OAUTH){
            authInProgress = false;
            if(resultCode == RESULT_OK) {

                if( !mApiClient.isConnecting() && !mApiClient.isConnected()){
                    mApiClient.connect();
                    Log.e("GoogleFit", "RESULT_OK");
                    Toast.makeText(getApplicationContext(),"Authentication successful.", Toast.LENGTH_SHORT).show();
                }
            } else  if( resultCode == RESULT_CANCELED ) {
                Log.e("GoogleFit", "RESULT_CANCELLED");
                Toast.makeText(getApplicationContext(),"Authentication cancelled.", Toast.LENGTH_SHORT).show();
            }
        }else {
            Log.e("GoogleFit", "requestCode NOT request_oauth");
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "Google API Client connected");

        Utility_GoogleFitSensors sensorsApi = Utility_GoogleFitSensors.getInstance();
        sensorsApi.create_StepSensor(mApiClient,this);
        //sensorsApi.findDataSources(mApiClient, this);

        //new insertGoogleDataSet().execute();
        Intent mainActivityIntent = new Intent(this, Activity_MainActivity.class);
        //mainActivityIntent.putExtra(this.GOOGLEFITCLIENT, (Serializable) this.mApiClient);
        startActivity(mainActivityIntent);
        finish();
    }



    @Override
    public void onConnectionSuspended(int i) {
        if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
            Log.i("GoogleFit", "Connection lost.  Cause: Network Lost.");

        } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
            Log.i("GoogleFit", "Connection lost.  Reason: Service Disconnected");
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
        if(!authInProgress){
            try{
                authInProgress = true;
                connectionResult.startResolutionForResult(this, REQUEST_OAUTH);
            }catch (IntentSender.SendIntentException e){
                Log.e("GoogleFit", "Connection not possible");
            }
        } else {
            Log.e("GoogleFit", "authInProgress");
        }
    }
}
