package de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alex on 08.06.2016.
 */
public class ActivitiesHeartIntraday {

    @SerializedName("datasetInterval")
    private int datasetInterval;

    @SerializedName("datasetType")
    private String datasetType;

    @SerializedName("dataset")
    private Dataset[] dataset;

    public ActivitiesHeartIntraday(Dataset[] dataset, int datasetInterval, String datasetType) {
        this.dataset = dataset;
        this.datasetInterval = datasetInterval;
        this.datasetType = datasetType;
    }

    public int getDatasetInterval() {
        return datasetInterval;
    }

    public String getDatasetType() {
        return datasetType;
    }

    public Dataset[] getDataset() {
        return dataset;
    }
}
