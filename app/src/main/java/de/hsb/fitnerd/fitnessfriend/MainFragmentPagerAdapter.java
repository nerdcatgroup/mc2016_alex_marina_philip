package de.hsb.fitnerd.fitnessfriend;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by alex on 26.05.2016.
 */
public class MainFragmentPagerAdapter extends FragmentPagerAdapter {

    private final Fragment_Overview fragment_overview;
    private final Fragment_Food fragment_food;
    private final Fragment_Debug fragment_debug;
    private final Fragment_Map fragment_map;
    private final GoogleApiClient mApiClient;

    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[]{"Overview", "Food", "Debug", "Map"};
    private Context context;

    public MainFragmentPagerAdapter(FragmentManager fm, Context context, GoogleApiClient apiClient) {
        super(fm);
        this.context = context;
        //this.fragment_map = new Fragment_Map().newInstance(apiClient);
        this.fragment_map = null;
        this.fragment_overview = new Fragment_Overview().newInstance(apiClient);
        this.fragment_food = new Fragment_Food();
        this.fragment_debug = new Fragment_Debug().newInstance(apiClient);

        this.mApiClient = apiClient;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return this.fragment_overview;
            case 1:
                return this.fragment_food;
            case 2:
                return this.fragment_debug;
            case 3:
                return this.fragment_map;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
