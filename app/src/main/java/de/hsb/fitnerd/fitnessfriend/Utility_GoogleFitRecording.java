package de.hsb.fitnerd.fitnessfriend;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;


/**
 * Test methods for Google's Recording API
 * @author Alexander Hähnel
 * @author Alphabet Inc.
 * @version 1.0
 */
public class Utility_GoogleFitRecording {
    private  static Utility_GoogleFitRecording self;
    private final static String TAG = "GoogleFitHistory";


    private Utility_GoogleFitRecording(){}

    public static Utility_GoogleFitRecording getInstance() {

        if(self == null){
            self = new Utility_GoogleFitRecording();
        }

        return self;
    }

    /**
     * Subscribes all DataSources with the given dataType. The Google Recording API will then start
     * to save all data of the data streams to the Google Fitness Store.
     * @param client The current Google API client
     * @param dataType The DataType to subscribe
     */
    public void subscribeDataType(GoogleApiClient client, DataType dataType) {

        Fitness.RecordingApi.subscribe(client, dataType)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            if (status.getStatusCode()
                                    == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                                Log.i(TAG, "Existing subscription for this data type detected.");
                            } else {
                                Log.i(TAG, "Successfully subscribed.");
                            }
                        } else {
                            Log.i(TAG, "There was a problem subscribing.");
                        }
                    }
                });
    }

    /**
     * Subscribe the given dataSource. The Google Recording API will then start
     * to save all data of the data stream to the Google Fitness Store.
     * @param client The current Google API client
     * @param dataSource The DataSource to subscribe
     */
    public void subscribeDataSource(GoogleApiClient client, DataSource dataSource) {

        Fitness.RecordingApi.subscribe(client, dataSource)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            if (status.getStatusCode()
                                    == FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                                Log.i(TAG, "Existing subscription for this data source detected.");
                            } else {
                                Log.i(TAG, "Successfully subscribed.");
                            }
                        } else {
                            Log.i(TAG, "There was a problem subscribing.");
                        }
                    }
                });
    }

    /**
     * Unsubscribes all DataSources of the given DataType. The Google Recording API will then stop
     * to save data to the Google Fitness Store.
     * @param client The current Google API client
     * @param dataType The DataType to unsubscribe.
     */
    public void removeDataTypeSubscription(GoogleApiClient client, final DataType dataType) {

        Fitness.RecordingApi.unsubscribe(client, dataType)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.i(TAG, "Successfully unsubscribed for data type: " + dataType.toString());
                        } else {
                            Log.i(TAG, "Failed to unsubscribe for data type: " + dataType.toString());
                        }
                    }
                });
    }


    /**
     * Unsubscribe the given DataSource. The Google Recording API will then stop
     * to save data to the Google Fitness Store.
     * @param client The current Google API client
     * @param dataSource The DataSource to unsubscribe.
     */
    public void removeDataSourceSubscription(GoogleApiClient client, final DataSource dataSource) {

        Fitness.RecordingApi.unsubscribe(client, dataSource)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.i(TAG, "Successfully unsubscribed for data data source: " + dataSource.toString());
                        } else {
                            Log.i(TAG, "Failed to unsubscribe for data source: " + dataSource.toString());
                        }
                    }
                });
    }



}
