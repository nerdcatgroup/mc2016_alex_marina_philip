package de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by Alex on 08.06.2016.
 */
public class HeartData {
    //private AactivitiesHeart[] aactivitiesHeart;

    @SerializedName("activities-heart-intraday")
    private ActivitiesHeartIntraday activitiesHeartIntraday;

    public HeartData(Object data, ActivitiesHeartIntraday activitiesHeartIntraday){
        this.activitiesHeartIntraday = activitiesHeartIntraday;
    }

    public ActivitiesHeartIntraday getActivitiesHeartIntraday() {
        return activitiesHeartIntraday;
    }
}
