package de.hsb.fitnerd.fitnessfriend;

import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import com.google.android.gms.common.api.GoogleApiClient;

public class Activity_MainActivity extends AppCompatActivity implements Fragment_Map.OnFragmentInteractionListener {

    private GoogleApiClient mApiClient;
    private Utility_FitBitConnection fitBitConnection;
    private Utility_FitBitConverter fitBitConverter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(Activity_GoogleFitLogin.mApiClient != null){
            this.mApiClient = Activity_GoogleFitLogin.mApiClient;
        }

        this.fitBitConnection = new Utility_FitBitConnection();
        this.fitBitConverter = new Utility_FitBitConverter();

        //DataSet dataset = Utility_GoogleFitHistory.GetInstance().createHeartDataSet(this.mApiClient, getApplicationContext(), this.fitBitConverter.fakeConverHeartJSON());
        //Utility_GoogleFitHistory.GetInstance().insertData(this.mApiClient, dataset);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MainFragmentPagerAdapter(getSupportFragmentManager(),
                Activity_MainActivity.this, mApiClient));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        this.fitBitConnection = new Utility_FitBitConnection();
        this.fitBitConverter = new Utility_FitBitConverter();
        /*
        this.fitBitConnection.execute(Utility_FitBitConnection.GetType.HEARTTODAY);
        try {
            HeartData testData = this.fitBitConverter.convertHeartJSON(this.fitBitConnection.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
