package de.hsb.fitnerd.fitnessfriend;

import android.content.Context;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.renderscript.Element;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.HistoryApi;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Value;
import com.google.android.gms.fitness.request.DataInsertRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataUpdateRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart.Dataset;
import de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart.HeartData;

/**
 * Created by Alex on 13.06.2016.
 */
public class Utility_GoogleFitHistory {

    private static Utility_GoogleFitHistory self;
    private static String TAG = "GoogleFitHistory";

    private Utility_GoogleFitHistory() {
    }

    ;

    public static Utility_GoogleFitHistory GetInstance() {

        if (self == null) {
            self = new Utility_GoogleFitHistory();
        }

        return self;
    }

    public DataReadResult CreateFoodReadRequest(GoogleApiClient client, int calendarTimeType, int timeRange) throws ExecutionException, InterruptedException {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(calendarTimeType, -timeRange);
        long startTime = cal.getTimeInMillis();

        java.text.DateFormat dateFormat = DateFormat.getDateInstance();
        Log.i(TAG, "Range Start: " + dateFormat.format(startTime));
        Log.i(TAG, "Range End: " + dateFormat.format(endTime));

        DataReadRequest dataReadRequest = new DataReadRequest.Builder()
                .read(DataType.TYPE_NUTRITION)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        return new requestDataSet(client).execute(dataReadRequest).get();
    }

    public DataReadResult CreateDataReadRequest(GoogleApiClient client, int calendarTimeType, int timeRange, TimeUnit bucketTimeUnit, int bucketTimeInterval, DataType input, DataType output) throws ExecutionException, InterruptedException {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(calendarTimeType, -timeRange);
        long startTime = cal.getTimeInMillis();

        java.text.DateFormat dateFormat = DateFormat.getDateInstance();
        Log.i(TAG, "Range Start: " + dateFormat.format(startTime));
        Log.i(TAG, "Range End: " + dateFormat.format(endTime));

        DataReadRequest dataReadRequest = new DataReadRequest.Builder()
                .aggregate(input, output)
                .bucketByTime(bucketTimeInterval, bucketTimeUnit)
                .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        return new requestDataSet(client).execute(dataReadRequest).get();
    }

    public DailyTotalResult getDailySteps(GoogleApiClient client) throws ExecutionException, InterruptedException {

        DailyTotalResult dailyTotalResult = new requestDailyTotalSteps(client).get();

        return dailyTotalResult;

    }

    public void updateDataSet(GoogleApiClient client, Context mContext) {
        // Set a start and end time for the data that fits within the time range
// of the original insertion.
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        cal.add(Calendar.DAY_OF_WEEK, -1);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.DAY_OF_WEEK, -2);
        long startTime = cal.getTimeInMillis();

// Create a data source
        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(mContext)
                .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
                .setStreamName(TAG + " - step count")
                .setType(DataSource.TYPE_RAW)
                .build();

// Create a data set
        int stepCountDelta = 1000;
        DataSet dataSet = DataSet.create(dataSource);
// For each data point, specify a start time, end time, and the data value -- in this case,
// the number of new steps.
        DataPoint dataPoint = dataSet.createDataPoint()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        dataPoint.getValue(Field.FIELD_STEPS).setInt(stepCountDelta);
        dataSet.add(dataPoint);

        Log.i(TAG, "Updating the dataset in the History API.");


        DataUpdateRequest request = new DataUpdateRequest.Builder()
                .setDataSet(dataSet)
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .build();

        new updateDataSet(client).execute(request);
    }

    public void createDataSet(GoogleApiClient client, Context mContext, int hoursAgo, DataType dataType, int value, Field fieldTypeInt) throws ExecutionException, InterruptedException {
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);
        long endTime = cal.getTimeInMillis();
        cal.add(Calendar.HOUR_OF_DAY, -hoursAgo);
        long startTime = cal.getTimeInMillis();

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(mContext)
                .setDataType(dataType)
                .setStreamName(TAG + " - " + dataType.toString())
                .setType(DataSource.TYPE_RAW)
                .build();

        DataSet dataSet = DataSet.create(dataSource);

        DataPoint dataPoint = dataSet.createDataPoint()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS);
        dataPoint.getValue(fieldTypeInt).setInt(value);
        dataSet.add(dataPoint);

        new insertDataSet(client).execute(dataSet);
        CreateDataReadRequest(client, Calendar.DAY_OF_WEEK, 1, TimeUnit.HOURS, 1, DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA);
    }

    public void createFoodDataSet(GoogleApiClient client, Context mContext, String foodName, int fieldMealType, int calories) throws ExecutionException, InterruptedException {
        Date now = new Date();

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(mContext)
                .setDataType(DataType.TYPE_NUTRITION)
                .setStreamName(TAG + " - Food")
                .setType(DataSource.TYPE_RAW)
                .build();

        DataSet dataSet = DataSet.create(dataSource);

        DataPoint dataPoint = dataSet.createDataPoint();
        dataPoint.setTimestamp(now.getTime(), TimeUnit.MILLISECONDS);
        dataPoint.getValue(Field.FIELD_FOOD_ITEM).setString(foodName);
        dataPoint.getValue(Field.FIELD_MEAL_TYPE).setInt(fieldMealType);
        dataPoint.getValue(Field.FIELD_NUTRIENTS).setKeyValue(Field.NUTRIENT_CALORIES, calories);
        //dataPoint.getValue(Field.FIELD_NUTRIENTS).setKeyValue();
        //dataPoint.getValue(Field.FIELD_NUTRIENTS).setKeyValue();
        dataSet.add(dataPoint);

        new insertDataSet(client).execute(dataSet);
        CreateFoodReadRequest(client, Calendar.DAY_OF_WEEK, 1);
    }


    private static void dumpBucket(List<Bucket> buckets) {

        for (Bucket buck : buckets) {
            Log.i(TAG, "Data returned for Bucket type: " + buck.getBucketType());

            DateFormat dateFormat = DateFormat.getDateInstance();
            //dataSet.getDataSet(DataType.TYPE_STEP_COUNT_DELTA).getDataPoints();
            for (DataSet ds : buck.getDataSets()) {
                for (DataPoint dp : ds.getDataPoints()) {
                    Log.i(TAG, "Data point:");
                    Log.i(TAG, "\tType: " + dp.getDataType().getName());
                    Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                    Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                    for (Field field : dp.getDataType().getFields()) {
                        Log.i(TAG, "\tField: " + field.getName() +
                                " Value: " + dp.getValue(field));
                    }
                }
            }
        }
    }

    private static void dumpDataSet(List<DataSet> dataSets) {

        for (DataSet ds : dataSets) {
            Log.i(TAG, "Data returned for Bucket type: " + ds.getDataType());

            DateFormat dateFormat = DateFormat.getDateInstance();
            //dataSet.getDataSet(DataType.TYPE_STEP_COUNT_DELTA).getDataPoints();
            for (DataPoint dp : ds.getDataPoints()) {
                    Log.i(TAG, "Data point:");
                    Log.i(TAG, "\tType: " + dp.getDataType().getName());
                    Log.i(TAG, "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
                    Log.i(TAG, "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)));
                    for (Field field : dp.getDataType().getFields()) {
                        Log.i(TAG, "\tField: " + field.getName() +
                                " Value: " + dp.getValue(field));
                    }
                Log.i(TAG, "Counter: " + ds.getDataPoints().size());
            }
        }
    }

    public DataSet createHeartDataSet(GoogleApiClient client, Context context, HeartData heartData) {

        Calendar cal = Calendar.getInstance();
        Date now = heartData.getActivitiesHeartIntraday().getDataset()[0].getTime();
        cal.setTime(now);

        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(context)
                .setDataType(DataType.TYPE_HEART_RATE_BPM)
                .setStreamName(TAG + " - heart rate")
                .setType(DataSource.TYPE_RAW)
                .build();

        DataSet dataSet = DataSet.create(dataSource);

        int count = 0;
        for (Dataset heatDataSet : heartData.getActivitiesHeartIntraday().getDataset()) {

            if (count >= 1000) {
                break;
            }

            cal.set(Calendar.SECOND, heatDataSet.getTime().getSeconds());
            cal.set(Calendar.MINUTE, heatDataSet.getTime().getMinutes());
            cal.set(Calendar.HOUR_OF_DAY, heatDataSet.getTime().getHours());
            String a = new SimpleDateFormat("dd.MM.yyyy - HH:mm.ss", Locale.GERMAN).format(cal.getTime());

            long time = cal.getTimeInMillis();

            DataPoint dataPoint = dataSet.createDataPoint()
                    .setTimestamp(time, TimeUnit.MILLISECONDS);
            dataPoint.getValue(Field.FIELD_BPM).setFloat(heatDataSet.getValue());

            dataSet.add(dataPoint);
            count++;
        }

        return dataSet;
    }

    public void insertData(GoogleApiClient client, DataSet dataset) {
        new insertDataSet(client).execute(dataset);
    }

    public class requestDataSet extends AsyncTask<DataReadRequest, Dataset, DataReadResult> {

        private GoogleApiClient mApiClient;

        public requestDataSet(GoogleApiClient client) {
            this.mApiClient = client;
        }

        @Override
        protected DataReadResult doInBackground(DataReadRequest... params) {
            DataReadResult result = Fitness.HistoryApi.readData(mApiClient, params[0]).await(1, TimeUnit.MINUTES);
            //Fitness.HistoryApi.readDailyTotal(mApiClient, params[0].getDataTypes().get(0));
                dumpDataSet(result.getDataSets());
            return result;
        }
    }

    public class requestDailyTotalSteps extends AsyncTask<DataReadRequest, Dataset, DailyTotalResult> {

        private GoogleApiClient mApiClient;

        public requestDailyTotalSteps(GoogleApiClient client) {
            this.mApiClient = client;

        }

        @Override
        protected DailyTotalResult doInBackground(DataReadRequest... params) {
            DailyTotalResult result = Fitness.HistoryApi.readDailyTotal(mApiClient, DataType.AGGREGATE_STEP_COUNT_DELTA).await(1, TimeUnit.MINUTES);
            //Fitness.HistoryApi.readDailyTotal(mApiClient, params[0].getDataTypes().get(0));
            return result;
        }
    }

    public class insertDataSet extends AsyncTask<DataSet, DataPoint, Boolean> {

        private GoogleApiClient mApiClient;

        public insertDataSet(GoogleApiClient client) {
            super();
            this.mApiClient = client;
        }

        @Override
        protected Boolean doInBackground(DataSet... params) {
            Log.i(TAG, "Trying to insert data in the background.");
            com.google.android.gms.common.api.Status insertStatus = Fitness.HistoryApi.insertData(mApiClient, params[0]).await(1, TimeUnit.MINUTES);
            if (!insertStatus.isSuccess()) {
                Log.i(TAG, "There was a problem inserting the dataset.");
                return null;
            }

            Log.i(TAG, "Data insert was successful!");
            return true;
        }
    }

    public class updateDataSet extends AsyncTask<DataUpdateRequest, DataPoint, Boolean> {

        private GoogleApiClient mApiClient;

        public updateDataSet(GoogleApiClient client) {
            super();
            this.mApiClient = client;
        }

        @Override
        protected Boolean doInBackground(DataUpdateRequest... params) {


            com.google.android.gms.common.api.Status updateStatus =
                    Fitness.HistoryApi.updateData(this.mApiClient, params[0])
                            .await(1, TimeUnit.MINUTES);

// Before querying the data, check to see if the update succeeded.
            if (!updateStatus.isSuccess()) {
                Log.i(TAG, "There was a problem updating the dataset.");
                return false;
            }

// At this point the data has been updated and can be read.
            Log.i(TAG, "Data update was successful.");
            return true;
        }
    }
}
