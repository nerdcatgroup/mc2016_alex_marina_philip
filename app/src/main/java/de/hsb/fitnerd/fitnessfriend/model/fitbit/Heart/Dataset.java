package de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Alex on 08.06.2016.
 */
public class Dataset {

    @SerializedName("time")
    private Date time;

    @SerializedName("value")
    private int value;
    private SimpleDateFormat timeFormat = new SimpleDateFormat ("H:mm:ss", Locale.GERMAN);

    public Dataset(Date time, int value){
        this.time = time;
    }


    public int getValue() {
        return value;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
