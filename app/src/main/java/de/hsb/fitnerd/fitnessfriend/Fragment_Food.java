package de.hsb.fitnerd.fitnessfriend;

import android.content.Intent;
import android.graphics.Outline;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.gms.fitness.data.Field;

import java.util.ArrayList;

import de.hsb.fitnerd.fitnessfriend.model.google.Food;

public class Fragment_Food extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    private Adapter_FoodAdapter foodAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPage = 2;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_food, container, false);

        Button fab = (Button) view.findViewById(R.id.fabbutton);
        ViewOutlineProvider viewOutlineProvider = new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                // Or read size directly from the view's width/height
                //int size = 20;
                int size = getResources().getDimensionPixelSize(R.dimen.fab_size);
                outline.setOval(0, 0, size, size);
            }
        };
        fab.setOutlineProvider(viewOutlineProvider);

        // Construct the data source
        ArrayList<Food> arrayOfFood = new ArrayList<Food>();
        // Create the adapter to convert the array to views
        this.foodAdapter = new Adapter_FoodAdapter(getContext(), arrayOfFood);
        // Attach the adapter to a ListView
        ListView listView = (ListView) view.findViewById(R.id.foodListView);
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        this.foodAdapter.add(new Food("Banane", null, Field.MEAL_TYPE_SNACK));
        listView.setAdapter(this.foodAdapter);

        Button addbutton = (Button) view.findViewById(R.id.fabbutton);
        addbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Activity_AddFood.class));
            }
        });
        return view;
    }
}
