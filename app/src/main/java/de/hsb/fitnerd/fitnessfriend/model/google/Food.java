package de.hsb.fitnerd.fitnessfriend.model.google;

import com.google.android.gms.fitness.data.Field;

import java.util.Map;

/**
 * Created by alex on 20.06.2016.
 */
public class Food {
    private String foodName;
    private Map<String, Float> nutrients;
    private int mealType;

    public Food(String foodName, Map<String, Float> nutrients, int metalType){
        this.foodName = foodName;
        this.nutrients = nutrients;
        this. mealType = metalType;
    }

    public String getFoodName() {
        return foodName;
    }

    public Map<String, Float> getNutrients() {
        return nutrients;
    }

    public int getMealType() {
        return mealType;
    }
}
