package de.hsb.fitnerd.fitnessfriend;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DataReadResult;

import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Fragment_Debug extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    private static GoogleApiClient mAPIClient;
    private DataReadResult mResult;
    Utility_FitBitConverter fitBitConverter;

    public static Fragment_Debug newInstance(GoogleApiClient client){
        Fragment_Debug fragmentLocation = new Fragment_Debug();
        /*Bundle bundle = new Bundle();
        bundle.putSerializable("apiClient", client);
        fragmentLocation.setArguments(bundle);
        */
        mAPIClient = client;
        return  fragmentLocation;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPage = 0;
        this.fitBitConverter = new Utility_FitBitConverter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_debug, container, false);
        //GoogleApiClient googleApiClient = ((GoogleFitApiClient) this.getArguments().getSerializable("apiClient")).mApiClient;
        final Utility_GoogleFitHistory history = Utility_GoogleFitHistory.GetInstance();
        Button getStepsButton = (Button) view.findViewById(R.id.location_getStepButton);
        assert getStepsButton != null;
        getStepsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    history.createFoodDataSet(mAPIClient,getContext(), "Banane", Field.MEAL_TYPE_SNACK, 60);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Button getUpdateButton = (Button) view.findViewById(R.id.update_data_button);
        assert getUpdateButton != null;
        getUpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    history.updateDataSet(mAPIClient, getContext());
            }
        });

        Button getFoodButton = (Button) view.findViewById(R.id.get_food_data_button);
        assert getFoodButton != null;
        getFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    history.CreateFoodReadRequest(mAPIClient, Calendar.WEEK_OF_YEAR, 1);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Button convertHeartDataButton = (Button) view.findViewById(R.id.convertHeartDataButton);
        convertHeartDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility_GoogleFitHistory history = Utility_GoogleFitHistory.GetInstance();
                DataSet dataSet = history.createHeartDataSet(mAPIClient, getContext(), fitBitConverter.fakeConverHeartJSON());
                history.insertData(mAPIClient, dataSet);
                Toast toast = Toast.makeText(getContext(), "Import heart data successful",Toast.LENGTH_SHORT);
                toast.show();
            }
        });

        Switch recordingSwitch = (Switch) view.findViewById(R.id.recordingSwitcher);
        recordingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Utility_GoogleFitRecording.getInstance().subscribeDataType(mAPIClient, DataType.TYPE_HEART_RATE_BPM);
                    Toast toast = Toast.makeText(getContext(), "Subscribe successful",Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Utility_GoogleFitRecording.getInstance().removeDataTypeSubscription(mAPIClient, DataType.TYPE_HEART_RATE_BPM);
                    Toast toast = Toast.makeText(getContext(), "Unsubscribe successful",Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        return view;
    }

}
