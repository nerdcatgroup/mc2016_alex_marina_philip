package de.hsb.fitnerd.fitnessfriend;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.fitness.data.Field;

import java.util.ArrayList;

import de.hsb.fitnerd.fitnessfriend.model.google.Food;

/**
 * Created by alex on 20.06.2016.
 */
public class Adapter_FoodAdapter extends ArrayAdapter<Food> {

    public Adapter_FoodAdapter(Context context, ArrayList<Food> food) {
        super(context, 0, food);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Food food = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.food_item, parent, false);
        }
        // Lookup view for data population
        TextView foodName = (TextView) convertView.findViewById(R.id.foodName);
        TextView footType = (TextView) convertView.findViewById(R.id.foodType);
        // Populate the data into the template view using the data object
        foodName.setText(food.getFoodName());
        switch (food.getMealType()){
            case Field.MEAL_TYPE_BREAKFAST:
                footType.setText("Breafast");
                break;
            case Field.MEAL_TYPE_DINNER:
                footType.setText("Dinner");
                break;
            case Field.MEAL_TYPE_LUNCH:
                footType.setText("Lunch");
                break;
            case Field.MEAL_TYPE_SNACK:
                footType.setText("Snack");
                break;
            case Field.MEAL_TYPE_UNKNOWN:
                footType.setText("Unknown");
                break;
            default:
                footType.setText("None");
        }
        // Return the completed view to render on screen
        return convertView;
    }
}
