package de.hsb.fitnerd.fitnessfriend.model.fitbit.Heart;

import com.google.android.gms.common.api.GoogleApiClient;

import java.io.Serializable;

/**
 * Created by User on 17.06.2016.
 */
public class GoogleFitApiClient implements Serializable {
    public GoogleApiClient mApiClient;

    public GoogleFitApiClient(GoogleApiClient client){
        this.mApiClient = client;
    }
}
